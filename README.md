## EagleOwl FrontEnd Assignment
#### Details
**Completed Tasks**
- [x] Integrate lazy-loading in the list component.
- [x] The checkbox on the column of the list component should select all the loaded rows.
- [x] Some hovering interactions should be added to the table rows.
### API List 

**Recipe List**
- [x] page: integer (mandatory)
- [x] is_incorrect: true / false
- [x] is_untagged: true / false
- [x] id_disabled: true / false
- [x] tags/action data is not provided in the response so any dummy value can be put   


**Recipe by margin**

*API url not working*

![RecipeMargin](/Screenshots/RecipeMargin.png)

**Recipe by fluctuation**

*API url not working*

![RecipeFluctuation](/Screenshots/RecipeFluctuation.png)
