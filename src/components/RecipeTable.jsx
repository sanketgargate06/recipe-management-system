import React from 'react'
import { Table , InputGroup} from "react-bootstrap";
import './RecipeTable.css';
import { RecipeLastUpdated } from './RecipeLastUpdated';
import { useStateValue } from '../StateProvider';
import { RECIPES_LOADED } from '../actionTypes'
import { RecipeTags } from './RecipeTags';
import { ColumnSort } from './ColumnSort';

export const RecipeTable = ( ) => {
    const [{ recipes }, dispatch] = useStateValue();

    const toggleAll = (e) => {
        const updated = [...recipes].map(recipe => {
                recipe.select = e.target.checked
                return recipe;
        })
        dispatch({
            type: RECIPES_LOADED,
            recipes: updated
        })
    }

    const toggle = (item) => {
        const updated = [...recipes].map(recipe => {
            if (recipe.id === item.id) {
                recipe.select = !recipe.select
            }
            return recipe;
        })
        dispatch({
            type: RECIPES_LOADED,
            recipes: updated
        })
    }

    return (
        <div className="recipeTable">
            <Table responsive="md" striped hover>
                <thead>
                    <tr>
                        <th className="recipeTable__header">
                            <div className="recipeTable__headerCheckBox">
                                <InputGroup.Checkbox onChange={(e) => {toggleAll(e)}}/>
                            </div>
                           </th>
                        <th className="recipeTable__header">
                            <div className="recipeTable__headerBox">
                                <span className="recipeTable__headerText">NAME </span>
                                <ColumnSort param="name"/>
                            </div>
                        </th>
                        <th className="recipeTable__header">
                            <div className="recipeTable__headerBox">
                                <span className="recipeTable__headerText">LAST UPDATED</span>
                                <ColumnSort param="last_updated"/>
                            </div>
                        </th>
                        <th className="recipeTable__header">
                            <div className="recipeTable__headerBox">
                                <span className="recipeTable__headerText">COGS%</span>
                                <ColumnSort wparam="cogs"/>
                            </div>
                        </th>
                        <th className="recipeTable__header">
                            <div className="recipeTable__headerBox">
                                <span className="recipeTable__headerText">COST PRICE()</span>
                                <ColumnSort type="int" param="cost_price"/>
                            </div>
                        </th>
                        <th className="recipeTable__header">
                            <div className="recipeTable__headerBox">
                                <span className="recipeTable__headerText">SALE PRICE</span>
                                <ColumnSort type="int" param="sale_price"/>
                            </div>
                        </th>
                        <th className="recipeTable__header">
                            <div className="recipeTable__headerBox">
                                <span className="recipeTable__headerText">GROSS MARGIN </span>
                                <ColumnSort type="int" param="gross_margin"/>
                            </div>
                        </th>
                        <th className="recipeTable__header">
                            <div className="recipeTable__headerTags">
                                TAGS/ACTIONS
                            </div>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    {recipes.map((recipe) => (
                        <tr key={recipe.id} className="recipeTable__item">
                            <td><InputGroup.Checkbox checked={recipe.select} onChange={() => {toggle(recipe)}}/></td>
                            <td>{recipe.name}</td>
                            <td><RecipeLastUpdated lastUpdated={recipe.last_updated.date}/></td>
                            <td>{recipe.cogs}%</td>
                            <td>{recipe.cost_price.toFixed(2)}</td>
                            <td>{recipe.sale_price.toFixed(2)}</td>
                            <td>{recipe.gross_margin.toFixed(2)}%</td>
                            <td className="recipeTable__tags"><RecipeTags tags={["Indian Masala", "Flavoured Rice","Home Delivery"]} /></td>     
                        </tr>
                    ))}
                </tbody>
            </Table>  
        </div>
    )
}

export default RecipeTable