import React from 'react'
import { MarginMeter } from './MarginMeter'
import './MonitorBox.css'

export const HighMargin = () => {
    return (
        <div className="monitorBox">
            <div className="monitorBox__header">High Margin Recipes</div>
            <div className="monitorBox__vitals">
                <MarginMeter name="Ambur Biryani" percentage="80" marginClass="marginMeter__high"/>
                <MarginMeter name="Paneer Tikka Masala" percentage="80" marginClass="marginMeter__high"/>
                <MarginMeter name="Palak Paneer Butter Masala" percentage="80" marginClass="marginMeter__high"/>
            </div>
        </div>
    )
}
