import React from 'react'
import { Fluctuation } from './Fluctuation'
import { HighMargin } from './HighMargin'
import { LowMargin } from './LowMargin'
import './ActivityMonitor.css'

export const ActivityMonitor = () => {
    return (
        <div className="activity__monitor">
            <HighMargin />
            <LowMargin />
            <Fluctuation />
        </div>
    )
}
