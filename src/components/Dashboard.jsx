import React from 'react'
import { ActivityMonitor } from './ActivityMonitor'
import { Recipes } from './Recipes'
import './Dashboard.css';

export const Dashboard = () => {
    return (
        <div className="dashBoard">
            <ActivityMonitor />
            <Recipes />
        </div>
    )
}
