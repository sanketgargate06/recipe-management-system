import React from 'react'
import './MarginArrow.css'
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';
import ArrowUpwardIcon from '@material-ui/icons/ArrowUpward';

export const MarginArrow = ({name, type, percentage}) => {
    return (
        <div className="marginArrow">
            <div className="marginArrow__titleBox">
                <h4 className="marginArrow__title">{name}</h4>
            </div>
            <div className="marginArrow__iconBox">
                {type ?
                <>
                <span className="marginArrow__uptext">{percentage}%</span>
                <span className="marginArrow__up"><ArrowUpwardIcon /> </span>
                </>:<>
                <span className="marginArrow__downtext">{percentage}%</span>
                <span className="marginArrow__down"><ArrowDownwardIcon /></span>
                </>
                }
            </div>
        </div>
    )
}
