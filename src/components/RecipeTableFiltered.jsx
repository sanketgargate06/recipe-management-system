import React from 'react'
import { Table , InputGroup} from "react-bootstrap";
import { useStateValue } from '../StateProvider';
import { ColumnSort } from './ColumnSort';
import { RecipeLastUpdated } from './RecipeLastUpdated';
import './RecipeTable.css';
import { RecipeTags } from './RecipeTags';
import { RECIPES_LOADED } from '../actionTypes'

const RecipeTableFiltered = ({ filterItem }) => {
    const [{ recipes }, dispatch] = useStateValue();

    const toggleAll = (e) => {
        const updated = [...recipes].map(recipe => {
                recipe.select = e.target.checked
                return recipe;
        })
        dispatch({
            type: RECIPES_LOADED,
            recipes: updated
        })
    }

    return (
        <div className="RecipeTableFiltered">
             <Table responsive="md" striped hover>
                <thead>
                <tr>
                        <th className="recipeTable__header">
                            <div className="recipeTable__headerCheckBox">
                                <InputGroup.Checkbox onChange={(e) => {toggleAll(e)}}/>
                            </div>
                           </th>
                        <th className="recipeTable__header">
                            <div className="recipeTable__headerBox">
                                <span className="recipeTable__headerText">NAME </span>
                                <ColumnSort param="name"/>
                            </div>
                        </th>
                        <th className="recipeTable__header">
                            <div className="recipeTable__headerBox">
                                <span className="recipeTable__headerText">LAST UPDATED</span>
                                <ColumnSort param="last_updated"/>
                            </div>
                        </th>
                        <th className="recipeTable__header">
                            <div className="recipeTable__headerBox">
                                <span className="recipeTable__headerText">COGS%</span>
                                <ColumnSort param="cogs"/>
                            </div>
                        </th>
                        <th className="recipeTable__header">
                            <div className="recipeTable__headerBox">
                                <span className="recipeTable__headerText">COST PRICE()</span>
                                <ColumnSort type="int" param="cost_price"/>
                            </div>
                        </th>
                        <th className="recipeTable__header">
                            <div className="recipeTable__headerBox">
                                <span className="recipeTable__headerText">SALE PRICE</span>
                                <ColumnSort type="int" param="sale_price"/>
                            </div>
                        </th>
                        <th className="recipeTable__header">
                            <div className="recipeTable__headerBox">
                                <span className="recipeTable__headerText">GROSS MARGIN </span>
                                <ColumnSort type="int" param="gross_margin"/>
                            </div>
                        </th>
                        <th className="recipeTable__header">
                            <div className="recipeTable__headerTags">
                                TAGS/ACTIONS
                            </div>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    {recipes.filter(recipe => recipe[filterItem] === true).map((recipe) => (
                        <tr key={recipe.id} className="recipeTable__item">
                            <td><InputGroup.Checkbox /></td>
                            <td>{recipe.name}</td>
                            <td><RecipeLastUpdated lastUpdated={recipe.last_updated.date}/></td>
                            <td>{recipe.cogs}%</td>
                            <td>{recipe.cost_price.toFixed(2)}</td>
                            <td>{recipe.sale_price.toFixed(2)}</td>
                            <td>{recipe.gross_margin.toFixed(2)} %</td>
                            <td><RecipeTags tags={["Indian Masala", "Rice"]} /></td>     
                        </tr>
                    ))}
                </tbody>
            </Table>  
        </div>
    )
}

export default RecipeTableFiltered