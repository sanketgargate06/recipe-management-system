import React from 'react'
import { Navbar } from 'react-bootstrap'
import './Header.css'

export const Header = () => {
    return (
        <Navbar bg="light" fixed="top">
                  <img alt="" src="https://eagleowl.in/w/wp-content/uploads/2019/03/cropped-favicon-96x96-192x192.png" width="50" height="50" className="d-inline-block align-top"/>
            <Navbar.Brand href="/">
        EagleOwl
    </Navbar.Brand>
  </Navbar>
    )
}
