import React, { Suspense, useState, useEffect } from 'react'
import './Recipes.css'
import {Tabs, Tab,Image} from "react-bootstrap";
import axiosInstance from '../axiosInstance';
import { useStateValue } from '../StateProvider';
import { RECIPES_LOADED } from '../actionTypes'


const RecipeTable = React.lazy(() => import('./RecipeTable'));
const RecipeTableFiltered = React.lazy(() => import('./RecipeTableFiltered'));

export const Recipes = () => {
    const [key, setKey] = useState('all');
    const [page, setPage] = useState('1');
    const [{ recipes }, dispatch] = useStateValue();

    const getRecipe = (page=1) => {
        axiosInstance
        .get(`recipes/?page=${page}`, {})
        .then(res => {
            dispatch({
                type: RECIPES_LOADED,
                recipes: res.data.results.map(recipe => {
                    return {
                        id : recipe.id,
                        select : false,
                        name: recipe.name,
                        last_updated: recipe.last_updated,
                        cogs : recipe.cogs,
                        cost_price: recipe.cost_price,
                        sale_price: recipe.sale_price,
                        gross_margin: recipe.gross_margin,
                        is_incorrect: recipe.is_incorrect,
                        is_untagged: recipe.is_untagged,
                        is_disabled: recipe.is_disabled
                    }
                })
            })
        })
        .catch((error) => {
            console.log(error)
        })
    }

    useEffect(() => {
        getRecipe();
      }, []);

    return (
    <React.Fragment>
        <div className="recipes">
            <Tabs id="controlled-tab-example" activeKey={key} onSelect={(k) => setKey(k)}>
                <Tab eventKey="all" title="ALL RECIPE(S)">
                    <Suspense fallback={<Spinner/>}>
                        <RecipeTable filterItem="None" />
                    </Suspense>
                </Tab>
                <Tab eventKey="incorrect" title="INCORRECT">
                    <Suspense fallback={<Spinner/>}>
                        <RecipeTableFiltered filterItem="is_incorrect"/>
                    </Suspense>
                </Tab>
                <Tab eventKey="untagged" title="UNTAGGED">
                    <Suspense fallback={<Spinner/>}>
                        <RecipeTableFiltered filterItem="is_untagged"/>
                    </Suspense>
                </Tab>
                <Tab eventKey="disabled" title="DISABLED">
                    <Suspense fallback={<Spinner/>}>
                        <RecipeTableFiltered filterItem="is_disabled"/>
                    </Suspense>
                </Tab>
            </Tabs>
        </div>
        <div className="recipesQuery">
            <span className="recipesQuery__text">Page:</span>
            <input list="pages" type="text" value={page} onChange={(e) => {getRecipe(e.target.value); setPage(e.target.value)}} className="recipesQuery__input"/>
            <datalist id="pages">
                <option value="1" />
                <option value="2" />
            </datalist>
        </div>
      
    </React.Fragment>
    )
}

const Spinner = () => {
    return (
    <Image
        src="http://cdn.lowgif.com/small/dc86e54ceca03be4-loading-spinner-animated-gif-83320-mediabin.gif"
        style={{ width: '100px', margin: 'auto', display: 'block' }}
        alt="loading..."
    />)
}