import moment from 'moment';
import React from 'react'

export const RecipeLastUpdated = ({lastUpdated}) => {
    
    return (
        <React.Fragment>
            {moment(lastUpdated).format("MMM DD, YYYY")}
        </React.Fragment>
    )
}
