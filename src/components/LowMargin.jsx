import React from 'react'
import { MarginMeter } from './MarginMeter'
import './MonitorBox.css'

export const LowMargin = () => {
    return (
        <div className="monitorBox">
            <h3 className="monitorBox__header">Low Margin Recipes</h3>
            <div className="monitorBox__vitals">
                <MarginMeter name="Ambur Biryani" percentage="48" marginClass="marginMeter__low"/>
                <MarginMeter name="Paneer Tikka Masala" percentage="25" marginClass="marginMeter__low"/>
                <MarginMeter name="Palak Paneer Butter Masala" percentage="15" marginClass="marginMeter__low"/>
            </div>
        </div>
    )
}
