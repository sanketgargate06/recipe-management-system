import React from 'react'
import { MarginArrow } from './MarginArrow'
import './MonitorBox.css'

export const Fluctuation = () => {
    return (
        <div className="monitorBox">
            <h3 className="monitorBox__header">Top Fluctuating Recipes</h3>
            <div className="monitorBox__vitals">
                <MarginArrow name="Ambur Biryani" type={true} percentage="5"/>
                <MarginArrow name="Paneer Tikka Masala" type={false} percentage="3"/>
                <MarginArrow name="Palak Paneer Butter Masala" type={false} percentage="8"/>
            </div>
        </div>
    )
}
