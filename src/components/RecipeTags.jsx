import React from 'react'
import { Badge } from 'react-bootstrap'
import './RecipeTags.css'

export const RecipeTags = ({tags}) => {
    return (
        <div className="recipeTags">
            {tags ? tags.map((tag) => (
                <span className="mx-1" key={tag}>
                    <Badge variant="warning">{tag}</Badge>
                </span>
                )) : 
                    <span>No tags</span>
                    }
        </div>
    )
}
