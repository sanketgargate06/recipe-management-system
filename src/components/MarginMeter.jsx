import React from 'react'
import './MarginMeter.css'
import { CircularProgressbar } from 'react-circular-progressbar';
import 'react-circular-progressbar/dist/styles.css';

export const MarginMeter = ({name, percentage, marginClass}) => {
    return (
       <div className="marginMeter">
           <div className="marginMeter__titleBox">
            <h4 className="marginMeter__title">{name}</h4>
           </div>
           <div className={marginClass}>
                <CircularProgressbar 
                value={percentage} 
                text={`${percentage}%`} 
               />
            </div>       
        </div>
    )
}

