import { IconButton } from '@material-ui/core'
import React from 'react'
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import './RecipeTable.css';
import { useStateValue } from '../StateProvider';
import { RECIPES_SORTED } from '../actionTypes';

export const ColumnSort = (params) => {
    const [{ recipes }, dispatch] = useStateValue();
    
    const sortAsc = (recipes, params) => {
        let sorted = []
        if(params['param'] === 'name') {
            sorted = [...recipes].sort((a,b) => (a.name > b.name) ? 1 : ((b.name > a.name) ? -1 : 0))
        }
        else if (params['param'] === 'last_updated') {
            sorted = [...recipes].sort((a, b) => new Date(a.last_updated.date) - new Date(b.last_updated.date));
        }
        else if (params['param'] === 'cogs') {
            sorted = [...recipes].sort((a, b) => a.cogs - b.cogs);
        }
        else if (params['param'] === 'cost_price') {
            sorted = [...recipes].sort((a, b) => a.cost_price - b.cost_price);
        }
        else if (params['param'] === 'sale_price') {
            sorted = [...recipes].sort((a, b) => a.sale_price - b.sale_price);
        }
        else if (params['param'] === 'gross_margin') {
            sorted = [...recipes].sort((a, b) => a.gross_margin - b.gross_margin);
        }
        dispatch({
            type: RECIPES_SORTED,
            recipes: sorted
        })
    }

    const sortDesc = (recipes, params ) => {
        let sorted = []
        if(params['param'] === 'name') {
            sorted = [...recipes].sort((a,b) => (a.name > b.name) ? -1 : ((b.name > a.name) ? 1 : 0))
        }
        else if (params['param'] === 'last_updated') {
            sorted = [...recipes].sort((a, b) => new Date(b.last_updated.date) - new Date(a.last_updated.date));
        }
        else if (params['param'] === 'cogs') {
            sorted = [...recipes].sort((a, b) => b.cogs - a.cogs);
        }
        else if (params['param'] === 'cost_price') {
            sorted = [...recipes].sort((a, b) => b.cost_price - a.cost_price);
        }
        else if (params['param'] === 'sale_price') {
            sorted = [...recipes].sort((a, b) => b.sale_price - a.sale_price);
        }
        else if (params['param'] === 'gross_margin') {
            sorted = [...recipes].sort((a, b) => b.gross_margin - a.gross_margin);
        }
        dispatch({
            type: RECIPES_SORTED,
            recipes: sorted
        })
    }

    return (
        <div className="recipeTable__headerSort">
             <IconButton color="primary" onClick={() => {sortAsc(recipes, params)}}>
                <KeyboardArrowUpIcon />
            </IconButton>
            <IconButton color="primary" onClick={() => {sortDesc(recipes, params)}} >
                <KeyboardArrowDownIcon />
            </IconButton>
        </div>
    )
}
