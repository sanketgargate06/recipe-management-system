import axios from 'axios';

const axiosInstance = axios.create({
    baseURL: 'https://beta.eagleowl.in/api/v1/mock/organization/18/outlet/18/recipe/'
});

export default axiosInstance;