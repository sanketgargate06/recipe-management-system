import {RECIPES_LOADED, RECIPES_SORTED} from './actionTypes'

export const initialState = {
    recipes: [],
}

const reducer = (state, action) => {
    switch(action.type) {
        case RECIPES_LOADED:
            return {
                ...state,
                recipes: action.recipes,
            }
        case RECIPES_SORTED:
            return {
                ...state,
                recipes: action.recipes,
            }
        default:
            return state;
    }
}

export default reducer;
